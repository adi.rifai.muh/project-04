import React from 'react';
import "./Login.css";

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nama: '',
            password: '',
            action: '/Home',
        };
    }

    handleChange(event) {
        const inputValueNama = event.target.value;
        const inputValuePass = event.target.value;
        this.setState({
            nama: inputValueNama,
            password: inputValuePass,
        });
    }

    handleSubmit() {
        if (this.state.password !== 'belovely') {
            alert('Password Anda Salah!');
            this.setState({ action: '/' });
        }
    }

    render() {
        return (
            <div class="container-fluid px-5 py-5">
        <div class="card mx-auto shadow bg-white rounded">
                <div className="row">
                    
                <div className="col-md-4 m-5">
                <div className="jumbotron bg-white text-center">
                <h2>Sistem Perencanaan Kerja Harian</h2>
                </div>

                        <form
                            className="form-group text-left"
                            action={this.state.action}
                            method="get"
                            onSubmit={() => {
                                this.handleSubmit();
                            }}>

                            <div className="input-group mb-5">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><svg xmlns="http://www.w3.org/2000/svg" width="21" height="21" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user">
                                    <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                                    <circle cx="12" cy="7" r="4"></circle>
                                </svg></span>
                            </div>
                                <input
                                    onChange={(event) => {
                                        this.handleChange(event);
                                    }}
                                    className="form-control form-control-lg"
                                    type="email"
                                    placeholder="Email"
                                    name="uname"
                                    id="uname"
                                    required
                                    value={this.state.value}
                                />
            
                            </div>

                            <div className="input-group mb-5">
                            <div className="input-group-prepend">
                            <span className="input-group-text" id="basic-addon1"><svg xmlns="http://www.w3.org/2000/svg" width="21" height="21" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-lock">
                                    <rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect>
                                    <path d="M7 11V7a5 5 0 0 1 10 0v4"></path>
                                </svg></span>
                            </div>
                                <input
                                    onChange={(event) => {
                                        this.handleChange(event);
                                    }}
                                    className="form-control form-control-lg"
                                    type="password"
                                    placeholder="Kata Sandi"
                                    name="password"
                                    id="password"
                                    required
                                    value={this.state.value}
                                />
                            </div>
                            <br />

                            <button className="btn btn-primary shadow btn-block btn-lg" type="submit">
                                Login
                            </button>
                        </form>

                        {/* <form action={this.state.action}
                            method="get"
                            onSubmit={() => {
                                this.handleSubmit(); }} >
                            <input type="email" placeholder="Username" className="inputLogin" name="uname" id="uname" onChange={(event) => {
                                        this.handleChange(event);
                                    }} required
                                    value={this.state.value} />
                            <input type="password" placeholder="Password" className="inputLogin" name="pword" id="pword" onChange={(event) => {
                                        this.handleChange(event);
                                    }} required
                                    value={this.state.value} />
                            <button className="buttonLogin" type="submit">
                                Login
                            </button>
                        </form> */}
                    </div>

                    <div className="col-md-6">
                        <img src="https://images.pexels.com/photos/1534924/pexels-photo-1534924.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" alt="gambar" height="620" width="900" />
                    </div>

                    </div>
                </div>
            </div>
        );
    }
}

export default Login;